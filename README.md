# Toppik

Android Project (Java) source for Boost Code Challenge


What this App is all about
--------------------------
This App was developed as a respond to a code challenge.

The challenge asks for a development of an (Reddit clone) App with these functionalities:
- Maintain a list of topics and its upvotes/downvotes 
- Allow the user to submit topics not exceeding 255 chars
- Allow the user to upvote or downvote a topic. The user may upvote or downvote the same topic multiple times. 
- Tapping a topic should expand that topic into a separate view (page) 
- Always return a list of top 20 topics (sorted by upvotes, descending) on the homepage 
- Design an in-memory data structure that stores the topics in memory without using data persistence


Basic App Architecture
--------------------------
The App utilized standard Android Apps conventions, which includes Activities, Fragments, Services and the such.

To be precise, a total of 2 Activities, 1 Fragment and 1 Service was developed to support all of the required functionalities.


The Basic Data Structure utilized
---------------------------------
The App will record information in which user submit a Topic, and whom can also upvotes or downvotes it to move it up the Topics list.

To this end, the author have come up with the following design for the App Data structure:

1. Each Topic stores these informations 
	- its Title / Description (255 characters max)
	- no of upvotes
	- no of downvotes

2. There will be one object instance called 'TopicQueue' that holds a list of Topics submitted by the user. This object also have several utility methods such as sort functions.


How datastore in this App works
-------------------------------
This app utilizes an unbounded service instance as a temporary place to store all of the information provided by the user. Hence, the data will always be cleared off by Dalvik VM everytime the App process is killed.

Activities (screens) in this App accessed the informations from the unbounded service directly, thruough referencing a static object, named TopicQueue which holds the actual Topics information.

However, to avoid Null Exceptions from happening, Activities will only accessed the datastore from the unbounded service after receiving broadcast intent messages from the unbounded service, telling them it is ok for them to do so. 


Utilized Design Patterns
------------------------
The following Design Patterns were used thru out the App:
- Singleton
  -> One static object holds information of all Topics entered by the user.
  
- Observer / Listener
  -> This design pattern is used in abundance thru out the App source. The main purpose of why this pattern was chosen is to facilitate information flow between UI Views and the exact Objects which holds the information inside the datastore mentioned before.


Limitations  
-----------
- There is no persistence implemented, so all information entered by user will be gone upon the App's process termination.
- Topics information cannot be deleted.


Tools used to develop this App
------------------------------
Android Studio version 3.1.4. 


Disclaimer
----------
This source might contain codes generated by Android Studio Integrated Development Environment (IDE); 
- All XML resource files are initially generated / created by Android Studio IDE upon project creation. However all of the layout XML files are modified by the author in the end.
- All Java source files in this Project are handcrafted by the Author. Exceptions are a sample unit test and one Android Instrumentation test files generated by Android Studio IDE upon project creation.

(c) 2019 fahmimokhtar.online

*Toppik is a wordplay of 'Topic', and also 'Top-Pick(s)'

