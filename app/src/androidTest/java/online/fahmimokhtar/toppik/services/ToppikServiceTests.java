package online.fahmimokhtar.toppik.services;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static junit.framework.Assert.assertTrue;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import online.fahmimokhtar.toppik.activities.ToppikMain;
import online.fahmimokhtar.toppik.models.Topic;

@RunWith(AndroidJUnit4.class)
public class ToppikServiceTests {
    @Rule
    public ActivityTestRule<ToppikMain> mActivityTestRule =
            new ActivityTestRule<>(ToppikMain.class);

    @Test
    public void checkService_isStarted() {
        assertTrue(ToppikService.IS_SERVICE_RUNNING);
    }

    @Test
    public void checkData_isAccesible() {
        assertTrue(ToppikService.topicQueue != null);
    }

    @Test
    public void checkData_NewTopic_canBeAdded() {
        Topic newTopic = new Topic().setTitle("This is a topic test");

        ToppikService.topicQueue.addTopic(newTopic);

        List<Topic> currentTopics = ToppikService.topicQueue.getTopTopics(1);

        assertTrue(currentTopics.get(0).getTitle().compareTo(newTopic.getTitle()) == 0);
    }
}
