package online.fahmimokhtar.toppik.activities;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.allOf;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import online.fahmimokhtar.toppik.R;
import online.fahmimokhtar.toppik.activities.test.ToppikMainTestActivity;


@RunWith(AndroidJUnit4.class)
public class ToppikMainTests {

    @Rule
    public ActivityTestRule<ToppikMainTestActivity> mActivityTestRule =
            new ActivityTestRule<>(ToppikMainTestActivity.class);


    @Test
    public void checkViewIsLoaded() {
        onView(withId(R.id.textView_title)).check(matches(withText("Top-Piks")));
    }

    @Test
    public void checkRecyclerViewTopisIsPopulated_1() {

        onView(withId(R.id.recycler_view_topics)).check(matches(hasChildCount(2)));
    }

    @Test
    public void checkRecyclerViewTopisIsPopulated_2() {
        onView(withId(R.id.fab)).perform(click());

        onView(withId(R.id.topic_title)).perform(typeTextIntoFocusedView("New Topic"));

        onView(withText("Add")).perform(click());

        onView(withId(R.id.recycler_view_topics)).check(matches(hasChildCount(3)));
    }


    @Test
    public void checkAddNewDialogIsWorking() {
        onView(withId(R.id.fab)).perform(click());

        onView(withId(R.id.linear_layout_add_topic)).check(matches(isDisplayed()));
    }

    @Test
    public void checkUpvoteButtonIsWorking() {
        onView(allOf ( withId(R.id.button_upvote) , withTagValue(equalTo("2")))).perform(click());

        onView(allOf ( withId(R.id.upvote_count) , withTagValue(equalTo("2")))).equals("4");
    }

    @Test
    public void checkDownvoteButtonIsWorking() {
        onView(allOf ( withId(R.id.button_downvote) , withTagValue(equalTo("1")))).perform(click());

        onView(allOf ( withId(R.id.downvote_count) , withTagValue(equalTo("1")))).equals("2");
    }
}
