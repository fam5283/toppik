package online.fahmimokhtar.toppik.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import online.fahmimokhtar.toppik.R;
import online.fahmimokhtar.toppik.activities.test.TopicDetailsTestActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TopicDetailsTests {
    @Rule
    public ActivityTestRule<TopicDetailsTestActivity> mActivityTestRule =
            new ActivityTestRule<>(TopicDetailsTestActivity.class);

    @Test
    public void checkInfoIsCorrect() {
        onView(withId(R.id.topic_title)).check(matches(withText("Test topic 1")));
    }

    @Test
    public void checkUpvoteButtonIsWorking() {
        onView(withId(R.id.button_upvote)).perform(click());

        onView(withId(R.id.upvote_count)).check(matches(withText("3")));
    }

    @Test
    public void checkDownvoteButtonIsWorking() {
        onView(withId(R.id.button_downvote)).perform(click());

        onView(withId(R.id.downvote_count)).check(matches(withText("2")));
    }
}
