package online.fahmimokhtar.toppik.activities.test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import online.fahmimokhtar.toppik.activities.TopicDetails;
import online.fahmimokhtar.toppik.models.Topic;
import online.fahmimokhtar.toppik.services.ToppikService;

public class TopicDetailsTestActivity extends TopicDetails {

    protected void setupTopikSetviceBroadcastReceiver(){
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ToppikService.ServiceStarted)) {

                    createDummyData();
                    currentTopic = ToppikService.topicQueue.getTopic(topicId);
                    populateTopicDetails();
                }

                if (intent.getAction().equals(ToppikService.TopicsChanged)) {
                    currentTopic = ToppikService.topicQueue.getTopic(topicId);
                    populateTopicDetails();
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ToppikService.ServiceStarted);
        filter.addAction(ToppikService.TopicsChanged);
        mLocalBroadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    private void createDummyData() {
        Topic topic1 = new Topic().setTitle("Test topic 1");
        ToppikService.topicQueue.addTopic(topic1);

        Topic topic2 = new Topic().setTitle("Test topic 2");
        ToppikService.topicQueue.addTopic(topic2);

        topic1.doUpVote().doUpVote().doDownVote();
        topic2.doUpVote().doUpVote().doUpVote().doDownVote().doDownVote();
    }


    protected void getExtras() {
        topicId = 1;

        if (mLocalBroadcastManager == null || broadcastReceiver == null)
            setupTopikSetviceBroadcastReceiver();

        if (!ToppikService.IS_SERVICE_RUNNING) {
            startToppikService();
        } else {
            ToppikService.topicQueue.notifyTopicChange();
        }


    }

    void startToppikService() {
        Intent service = new Intent(this, ToppikService.class);
        service.setAction(ToppikService.StartService);
        startService(service);
    }

}
