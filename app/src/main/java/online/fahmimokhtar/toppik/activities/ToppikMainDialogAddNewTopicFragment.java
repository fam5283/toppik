package online.fahmimokhtar.toppik.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import online.fahmimokhtar.toppik.R;

public class ToppikMainDialogAddNewTopicFragment extends DialogFragment {
    public EditText topicTitle;

    public interface OnAddTopicListenerToppikMain {
        void executeAddNewTopic();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        View v = inflater.inflate(R.layout.dialog_add_topic, null);

        builder.setTitle("New Topic");
        builder.setView(v);

        topicTitle = v.findViewById(R.id.topic_title);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ToppikMainDialogAddNewTopicFragment.OnAddTopicListenerToppikMain activity = (ToppikMainDialogAddNewTopicFragment.OnAddTopicListenerToppikMain) getActivity();
                activity.executeAddNewTopic();
            }
        }).setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }
}
