package online.fahmimokhtar.toppik.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import online.fahmimokhtar.toppik.R;
import online.fahmimokhtar.toppik.models.Topic;
import online.fahmimokhtar.toppik.services.ToppikService;

public class TopicDetails extends AppCompatActivity {

    protected Topic currentTopic;
    protected int topicId;
    boolean buttonOnclickSet;

    protected LocalBroadcastManager mLocalBroadcastManager;
    protected BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_details);

        buttonOnclickSet = false;

        getExtras();
        populateTopicDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mLocalBroadcastManager == null || broadcastReceiver == null)
            setupTopikSetviceBroadcastReceiver();
    }

    protected void setupTopikSetviceBroadcastReceiver() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ToppikService.TopicsChanged)) {
                    currentTopic = ToppikService.topicQueue.getTopic(topicId);
                    populateTopicDetails();
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ToppikService.TopicsChanged);
        mLocalBroadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    protected void populateTopicDetails() {
        if (currentTopic != null) {
            TextView topicTitle = findViewById(R.id.topic_title);
            TextView upvoteCount = findViewById(R.id.upvote_count);
            TextView downvoteCount = findViewById(R.id.downvote_count);
            Button buttonDownvote = findViewById(R.id.button_downvote);
            Button buttonUpvote = findViewById(R.id.button_upvote);

            topicTitle.setText(currentTopic.getTitle());
            upvoteCount.setText(currentTopic.getUpvotes()+ "" );
            downvoteCount.setText(currentTopic.getDownvotes()+ "" );

            if (!buttonOnclickSet) {
                buttonUpvote.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        currentTopic.doUpVote().notifyListener();
                    }
                });

                buttonDownvote.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        currentTopic.doDownVote().notifyListener();
                    }
                });
            }
        }
    }

    protected void getExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            topicId = extras.getInt("topicId");
            currentTopic = ToppikService.topicQueue.getTopic(topicId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
