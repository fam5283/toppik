package online.fahmimokhtar.toppik.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import online.fahmimokhtar.toppik.R;
import online.fahmimokhtar.toppik.datadapters.TopicCardAdapter;
import online.fahmimokhtar.toppik.interfaces.ToppikMainListener;
import online.fahmimokhtar.toppik.models.Topic;
import online.fahmimokhtar.toppik.services.ToppikService;

import static android.support.v4.app.DialogFragment.STYLE_NORMAL;

public class ToppikMain extends AppCompatActivity implements ToppikMainDialogAddNewTopicFragment.OnAddTopicListenerToppikMain, ToppikMainListener {

    public static RecyclerView mRecyclerView;
    public static RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    protected LocalBroadcastManager mLocalBroadcastManager;
    protected BroadcastReceiver broadcastReceiver;

    ToppikMainDialogAddNewTopicFragment newTopicDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toppik_main);

    }

    protected void setupTopikSetviceBroadcastReceiver() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ToppikService.ServiceStarted) || intent.getAction().equals(ToppikService.TopicsChanged)) {

                    populateTopicsView();
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ToppikService.ServiceStarted);
        filter.addAction(ToppikService.TopicsChanged);
        mLocalBroadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onResume() {

        super.onResume();
        if (mLocalBroadcastManager == null || broadcastReceiver == null)
            setupTopikSetviceBroadcastReceiver();

        if (!ToppikService.IS_SERVICE_RUNNING) {
            startToppikService();
        } else {
            ToppikService.topicQueue.notifyTopicChange();
        }
    }

    protected void populateTopicsView() {
        mRecyclerView = findViewById(R.id.recycler_view_topics);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new TopicCardAdapter(ToppikService.topicQueue.getTopTopics(20), this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void startToppikService() {
        Intent service = new Intent(this, ToppikService.class);
        service.setAction(ToppikService.StartService);
        startService(service);
    }

    @Override
    public void executeAddNewTopic() {
        Topic newTopic = new Topic().setTitle(newTopicDialog.topicTitle.getText().toString());
        ToppikService.topicQueue.addTopic(newTopic);
        newTopic.notifyListener();
        populateTopicsView();
    }

    public void doAddNewTopic(View view){
        final FragmentManager manager = getSupportFragmentManager();
        Fragment frag = manager.findFragmentByTag("newtopic_dialog");
        if (frag != null) {
            manager.beginTransaction().remove(frag).commitAllowingStateLoss();
        }

        newTopicDialog = new ToppikMainDialogAddNewTopicFragment();
        newTopicDialog.setStyle(STYLE_NORMAL, DialogFragment.STYLE_NORMAL);
        newTopicDialog.setCancelable(false);
        newTopicDialog.show(manager, "newtopic_dialog");
    }

    @Override
    public void openTopicDetail(int topicId) {
        Intent topicDetailsIntent = new Intent(this, TopicDetails.class);
        topicDetailsIntent.putExtra("topicId", topicId);
        startActivityForResult(topicDetailsIntent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100){
            populateTopicsView();
        }
    }
}
