package online.fahmimokhtar.toppik.datadapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import online.fahmimokhtar.toppik.R;
import online.fahmimokhtar.toppik.interfaces.ToppikMainListener;
import online.fahmimokhtar.toppik.models.Topic;

public class TopicCardAdapter extends RecyclerView.Adapter<TopicCardAdapter.ViewHolder> {

    private List<Topic> mDataset = new ArrayList<>();
    private ViewHolder vh;
    private Context context;
    private ToppikMainListener listener;

    public TopicCardAdapter(List<Topic> myDataset) {
        this.mDataset = myDataset;
    }

    public TopicCardAdapter(List<Topic> myDataset, ToppikMainListener toppikMainListener) {
        this.mDataset = myDataset;
        listener = toppikMainListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_topic_card_layout, parent, false);
        vh = new ViewHolder(v);
        context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        position = holder.getAdapterPosition();
        final int key = mDataset.get(position).getId();

        holder.titleText.setText(mDataset.get(position).getTitle());
        holder.upvoteCount.setText(mDataset.get(position).getUpvotes()+ "" );
        holder.upvoteCount.setTag(key+"");
        holder.downvoteCount.setText(mDataset.get(position).getDownvotes()+ "" );
        holder.downvoteCount.setTag(key+"");

        final Topic currentTopicObj = mDataset.get(position);

        holder.buttonUpvote.setTag(key+"");
        holder.buttonUpvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentTopicObj.doUpVote().notifyListener();
            }
        });

        holder.buttonDownvote.setTag(key+"");
        holder.buttonDownvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentTopicObj.doDownVote().notifyListener();
            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.openTopicDetail(currentTopicObj.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView titleText;
        public TextView upvoteCount;
        public Button buttonUpvote;
        public TextView downvoteCount;
        public Button buttonDownvote;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            titleText =  itemView.findViewById(R.id.text_topic_title);
            upvoteCount = itemView.findViewById(R.id.upvote_count);
            buttonUpvote = itemView.findViewById(R.id.button_upvote);
            downvoteCount = itemView.findViewById(R.id.downvote_count);
            buttonDownvote = itemView.findViewById(R.id.button_downvote);
        }
    }
}
