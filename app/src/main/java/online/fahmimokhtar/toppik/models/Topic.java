package online.fahmimokhtar.toppik.models;

import online.fahmimokhtar.toppik.interfaces.TopicQueueListener;

public class Topic {
    /*
       Topic Datastructure
    */

    private int id;
    private String Title;
    private int Upvotes;
    private int Downvotes;
    private int Position;


    // Placed a reference to TopicQueue instance so that the Sort method contained there can be called indirectly.
    private TopicQueueListener listener;

    // This is where the reference to TopicQueue instance  is set
    public void setTopicQueueListener(TopicQueueListener topicQueueListener) {
        listener = topicQueueListener;
    }

    public int getId() {
        return id;
    }


    public Topic setId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return Title;
    }

    public Topic setTitle(String title) {
        Title = title;
        return this;
    }

    public int getUpvotes() {
        return Upvotes;
    }

    public Topic setUpvotes(int upvotes) {
        Upvotes = upvotes;
        return this;
    }

    public int getDownvotes() {
        return Downvotes;
    }

    public Topic setDownvotes(int downvotes) {
        Downvotes = downvotes;
        return this;
    }

    public int getPosition() {
        return Position;
    }

    public Topic setPosition(int position) {
        Position = position;
        return this;
    }

    public int getWeight() {
        return getUpvotes() - getDownvotes();
    }

    // method to increase the number of Downvote incrementally (by 1)
    public Topic doDownVote() {
        Downvotes++;
        return this;
    }

    // method to increase the number of Upvote incrementally (by 1)
    public Topic doUpVote() {
        Upvotes++;
        return this;
    }

    // this is the method that will indirectly initiate the call to the Sort method contained in TopicQueue
    public void notifyListener() {
        listener.notifyTopicChange();
    }
}
