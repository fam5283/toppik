package online.fahmimokhtar.toppik.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import online.fahmimokhtar.toppik.interfaces.TopicQueueListener;
import online.fahmimokhtar.toppik.interfaces.TopicServiceListener;
import online.fahmimokhtar.toppik.utils.TopicComparatorAsc;
import online.fahmimokhtar.toppik.utils.TopicComparatorDesc;

 /*

    This class would served as the 'container' for all  Topics that are posted by the App user.
    Besides that, there are also some methods inside this Class that would be responsible to determine the position / ranking
    of each topic (via sorting)

 */

 // This class implements the TopicQueueListener Interface as a bridge between an instance of this Class to a Topic Instance
public class TopicQueue implements TopicQueueListener {

    /*
        TopicQueue Datastructure
    */

    // This is where all Posted Topics Data will live
    private List<Topic> topics;

    // contains the running number of Topic ids
    private int idRunningNumber;

    // flag to determine the sorting type; true for Ascending and false for Descending
    private boolean isSortAscending;

    // object that would served as 'Token' by synchronized
    public final Object MUTEX = new Object();


    private TopicServiceListener listener;

    public TopicQueue() {
        topics = new ArrayList<Topic>();
        idRunningNumber = 1;
    }

    public void setTopicServiceListener(TopicServiceListener topicServiceListener) {
        listener = topicServiceListener;
    }

    public void addTopic (Topic topic) {
        if (topic == null) throw new NullPointerException("Null Topic Object!");
        topic.setId(idRunningNumber++);

        // this is how the bridge between this and an instance of a Topic is established
        topic.setTopicQueueListener(this);

        // added to ensure Thread safety
        synchronized (MUTEX) {
            if (!topics.contains(topic)) {
                topics.add(topic);
            }
        }
    }

    public void removeTopic (Topic topic) {
        // added to ensure Thread safety
        synchronized (MUTEX) {
            topics.remove(topic);
        }
    }

    public void sortAsc() {
        // added to ensure Thread safety
        synchronized (MUTEX) {
            Collections.sort(topics, new TopicComparatorAsc());
        }

        updatePosition();
    }

    public void sortDesc() {
        // added to ensure Thread safety
        synchronized (MUTEX) {
            Collections.sort(topics, new TopicComparatorDesc());
        }

        updatePosition();
    }

    public void updatePosition() {
        int idx = 1;
        // added to ensure Thread safety
        synchronized (MUTEX) {
            for (Topic topic : topics) {
                // update position / rank property in each Topic
                topic.setPosition(idx++);
            }
        }
        listener.notifyTopicChange();
    }

    // this is how an Instance of a Topic can indirectly access the sort methods.
    @Override
    public void notifyTopicChange() {
        if (isSortAscending) sortAsc(); else sortDesc();
    }


    public boolean isSortAscending() {
        return isSortAscending;
    }

    public TopicQueue setSortAscending(boolean sortAscending) {
        isSortAscending = sortAscending;
        return this;
    }

    // an implementation to only return a list of N Topics
    public List<Topic> getTopTopics(int count) {
        return topics.stream().limit(count).collect(Collectors.<Topic>toList());
    }

    public Topic getTopic(int id){
        return topics.stream().filter(o -> o.getId() == id).findFirst().get();
    }
}
