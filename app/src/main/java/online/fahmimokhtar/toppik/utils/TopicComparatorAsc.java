package online.fahmimokhtar.toppik.utils;

import java.util.Comparator;

import online.fahmimokhtar.toppik.models.Topic;

/*
    This class contains the implementation of a comparator to do Sorting of topics
    based on number of Upvotes Ascendingly

 */
public class TopicComparatorAsc implements Comparator<Topic> {

    @Override
    public int compare(Topic o1, Topic o2) {
        return o1.getUpvotes() < o2.getUpvotes() ? -1 : o1.getUpvotes() == o2.getUpvotes() ? 0 : 1;
    }
}
