package online.fahmimokhtar.toppik.interfaces;

/*
    The interface to be implemented by TopicQueue
 */
public interface TopicQueueListener {
    public void notifyTopicChange();
}
