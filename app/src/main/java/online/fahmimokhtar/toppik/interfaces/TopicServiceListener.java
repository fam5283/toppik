package online.fahmimokhtar.toppik.interfaces;

public interface TopicServiceListener {
    public void notifyTopicChange();
}
