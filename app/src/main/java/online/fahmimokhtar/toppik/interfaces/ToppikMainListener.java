package online.fahmimokhtar.toppik.interfaces;

public interface ToppikMainListener {
    public void openTopicDetail(int topicId);
}
