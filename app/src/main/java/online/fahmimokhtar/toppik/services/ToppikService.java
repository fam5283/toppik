package online.fahmimokhtar.toppik.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import online.fahmimokhtar.toppik.interfaces.TopicServiceListener;
import online.fahmimokhtar.toppik.models.TopicQueue;

/*
    This is the declaration of an Unbounded Service that would served as a place where an TopicQueue Instance will live
    during the whole App's lifetime
 */

public class ToppikService extends Service implements TopicServiceListener {

    LocalBroadcastManager mLocalBroadcastManager;

    public static String StartService = "online.fahmimokhtar.toppik.STARTSERVICE";

    public static String ServiceStarted = "online.fahmimokhtar.toppik.SERVICESTARTED";

    public static String TopicsChanged = "online.fahmimokhtar.toppik.TOPICSCHANGED";

    public static boolean IS_SERVICE_RUNNING = false;

    // we make it public static so that it can be readily accessed  as long as this Service is still running
    public static TopicQueue topicQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        topicQueue = new TopicQueue();
        topicQueue.setTopicServiceListener(this);

        // ser the default prefered Topic sorting method to Descending
        topicQueue.setSortAscending(false);
        IS_SERVICE_RUNNING = true;
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null)
        {
            if (intent.getAction().equals(StartService)){
                Log.d("ToppikService", "ServiceStarted");

                Intent broadcastIntent = new Intent(ServiceStarted);
                mLocalBroadcastManager.sendBroadcast(broadcastIntent);
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void notifyTopicChange() {
        Log.d("ToppikService", "TopicsChanged");

        Intent broadcastIntent = new Intent(TopicsChanged);
        mLocalBroadcastManager.sendBroadcast(broadcastIntent);
    }
}
