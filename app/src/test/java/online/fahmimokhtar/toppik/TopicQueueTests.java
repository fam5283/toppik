package online.fahmimokhtar.toppik;

import org.junit.Test;

import online.fahmimokhtar.toppik.models.Topic;
import online.fahmimokhtar.toppik.models.TopicQueue;

import static org.junit.Assert.assertEquals;

public class TopicQueueTests {

    // To test whether null objects would be rejected if it tried to be added into TopicQueue
    @Test(expected = NullPointerException.class)
    public void TopicQueue_Reject_Null_Topic () {
        TopicQueue topicQueueTest = new TopicQueue();
        Topic topic = null;
        topicQueueTest.addTopic(topic);
    }

    // To test ascending sorting method in TopicQueue
    @Test
    public void TopicQueue_AscSort_isWorking () {
        TopicQueue topicQueueTest = new TopicQueue().setSortAscending(true);
        Topic topicA = new Topic().setTitle("Topic A").setUpvotes(0).setDownvotes(4);
        Topic topicB = new Topic().setTitle("Topic B").setUpvotes(3).setDownvotes(1);
        Topic topicC = new Topic().setTitle("Topic C").setUpvotes(4).setDownvotes(0);

        topicQueueTest.addTopic(topicA);
        topicQueueTest.addTopic(topicB);
        topicQueueTest.addTopic(topicC);

        topicQueueTest.sortAsc();

        assertEquals(3, topicC.getPosition());
    }

    // To test descending sorting method in TopicQueue
    @Test
    public void TopicQueue_DescSort_isWorking () {
        TopicQueue topicQueueTest = new TopicQueue().setSortAscending(false);
        Topic topicA = new Topic().setTitle("Topic A").setUpvotes(3).setDownvotes(4);
        Topic topicB = new Topic().setTitle("Topic B").setUpvotes(4).setDownvotes(1);
        Topic topicC = new Topic().setTitle("Topic C").setUpvotes(2).setDownvotes(0);

        topicQueueTest.addTopic(topicA);
        topicQueueTest.addTopic(topicB);
        topicQueueTest.addTopic(topicC);

        topicQueueTest.sortDesc();

        assertEquals(2, topicA.getPosition());
    }

    // To test whether the indirect call from Topic object of the sort method in TopicQueue is working as expected
    @Test
    public void TopicQueue_notifyTopicChange_isCalled () {
        TopicQueue topicQueueTest = new TopicQueue().setSortAscending(false);

        Topic topicA = new Topic().setTitle("Topic A").setUpvotes(3).setDownvotes(4);
        Topic topicB = new Topic().setTitle("Topic B").setUpvotes(4).setDownvotes(1);
        Topic topicC = new Topic().setTitle("Topic C").setUpvotes(2).setDownvotes(0);

        topicQueueTest.addTopic(topicA);
        topicQueueTest.addTopic(topicB);
        topicQueueTest.addTopic(topicC);

        topicQueueTest.sortDesc();

        topicA.doUpVote().doUpVote().notifyListener();

        assertEquals(1, topicA.getPosition());
    }

    @Test
    public void TopicQueue_getTopTopics_isWorking() {
        TopicQueue topicQueueTest = new TopicQueue().setSortAscending(false);

        Topic topicA = new Topic().setTitle("Topic A").setUpvotes(3).setDownvotes(4);
        Topic topicB = new Topic().setTitle("Topic B").setUpvotes(4).setDownvotes(1);
        Topic topicC = new Topic().setTitle("Topic C").setUpvotes(2).setDownvotes(0);

        topicQueueTest.addTopic(topicA);
        topicQueueTest.addTopic(topicB);
        topicQueueTest.addTopic(topicC);

        assertEquals( 2, topicQueueTest.getTopTopics(2).size());
    }
}
