package online.fahmimokhtar.toppik;

import org.junit.Test;

import online.fahmimokhtar.toppik.models.Topic;

import static org.junit.Assert.*;

public class TopicTests {

    @Test
    public void TopicDoUpVote_isCorrect() {
        Topic testTopic;
        testTopic = new Topic();
        testTopic.setUpvotes(3);
        assertEquals(4, testTopic.doUpVote().getUpvotes());
    }

    @Test
    public void TopicDoDownVote_isCorrect() {
        Topic testTopic;
        testTopic = new Topic();
        testTopic.setDownvotes(3);
        assertEquals(4, testTopic.doDownVote().getDownvotes());
    }

    @Test
    public void TopicGetWeight_isCorrect() {
        Topic testTopic;
        testTopic = new Topic();
        testTopic.setUpvotes(4);
        testTopic.setDownvotes(3);
        assertEquals(1, testTopic.getWeight());
    }
}
